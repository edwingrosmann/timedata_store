use std::cmp::min;
use std::io;
use std::io::Write;
use std::time::Duration;

use kafka::consumer::{Consumer, FetchOffset, GroupOffsetStorage, Message, MessageSet};
use kafka::error::Result;
use serde_json::from_slice;

use crate::mongo_api;
use crate::settings::{consumer_group, kafka_topic, test_mode};
use crate::time_and_date_data::{KafkaAction::{Delete, Upsert}, MessageKey, TimeData};

pub fn consume_topic(topic: &str, consumer_group: &str) {
    match &mut consumer(topic, consumer_group) {
        Ok(c) =>
            poll_forever(c),
        _ => {
            //Try again in a second...
            wait_a_second();
            consume_topic(&topic, &consumer_group);
        }
    }
}


fn poll_forever(consumer: &mut Consumer) {
    println!(
        "\nStarting to consume messages as Consumer-Group '{}' from Topic {:?}",
        consumer.group(), consumer.subscriptions()
    );

    //Ground-hog day anyone?...
    loop {
        poll(consumer);

        if test_mode() {
            return;
        }
    }
}

fn poll(consumer: &mut Consumer) {
    let message_sets = consumer
        .poll();

    match message_sets {
        Ok(m) =>
            m.iter()
                .for_each(|ms| consume_message_set(consumer, ms)),
        _ =>//The consumer-group is no longer available! Start a new session...
            consume_topic(&kafka_topic(), &consumer_group()),
    }
    commit_consumed(consumer);
}

fn consume_message_set(consumer: &mut Consumer, ms: MessageSet) {
    ms.messages().iter().for_each(process_message);

    consumer
        .consume_messageset(ms)
        .expect("Could not mark the just-processed Kafka MessageSet as 'Consumed'");
}

///The message should have a JSON body...
fn process_message(m: &Message) {
    let k = &String::from_utf8_lossy(m.key).to_string();

    println!(
        "Received Kafka Message: Key = {}; Payload = '{}...'",
        k,
        String::from_utf8_lossy(m.value)
            .split_at(min(m.value.len(), 700))
            .0
    );

    let k = &from_slice::<MessageKey>(&m.key).expect("Could not read Key from kafka-message");
    match &k.action {
        Upsert => mongo_api::store( &from_slice::<TimeData>(&m.value).expect("Could not create TimeData from kafka-message")),
        Delete => mongo_api::delete_by_key(&k.name),
    };
}

fn commit_consumed(consumer: &mut Consumer) {
    consumer
        .commit_consumed()
        .expect("Could Commit the Consumed MessageSet(s) to Zookeeper");
}

fn consumer(topic: &str, consumer_group: &str) -> Result<Consumer> {
    Consumer::from_hosts(vec!["localhost:9092".to_owned()])
        .with_topic(topic.to_owned())
        .with_group(consumer_group.to_owned())
        .with_fallback_offset(FetchOffset::Earliest)
        .with_offset_storage(GroupOffsetStorage::Kafka)
        .with_fetch_max_bytes_per_partition(200000)
        .create()
}

fn wait_a_second() {
    std::thread::sleep(Duration::from_millis(1000));
    print!(".");
    io::stdout().flush().unwrap();
}
