use crate::time_and_date_data::TimeData;
use mongodb::bson::{doc, Document};
use mongodb::error::Result;
use mongodb::results::DeleteResult;
use mongodb::Client;

pub fn store(data: &TimeData) {
    upsert(
        &create_mongo_client("localhost").expect("Could not create a MongoDB Client"),
        "test",
        "city_data",
        &mut bson::to_document(data).expect("Could not create Document-to-store from Time-Data"),
    )
    .unwrap_or_else(|_| panic!("Error Occurred whilst storing City-data with key '{}'", data._id));
}

pub fn delete_by_key(key: &str) {
    delete(
        &create_mongo_client("localhost").expect("Could not create a MongoDB Client"),
        "test",
        "city_data",
        key,
    )
    .unwrap_or_else(|_| {
        panic!(
            "Error Occurred whilst Deleting City-data with key '{}'",
            key
        )
    });
}

#[tokio::main]
async fn delete(
    client: &Client,
    database: &str,
    collection_name: &str,
    key: &str,
) -> Result<DeleteResult> {
    client
        .database(database)
        .collection(collection_name)
        .delete_one(doc! {"_id":key}, None)
        .await
}

#[tokio::main]
async fn create_mongo_client(host: &str) -> Result<Client> {
    Client::with_uri_str(format!("mongodb://{}:27017", host).as_str()).await
}

#[tokio::main]
async fn upsert(
    client: &Client,
    database: &str,
    collection_name: &str,
    data: &mut Document,
) -> Result<()> {
    let collection = client.database(database).collection(collection_name);

    //Find by key-field...
    let q = doc! {"_id":data.get_str("_id").unwrap()};

    //Upsert: If the document exists then update otherwise insert.
    //Empirical Observation: Updating and inserting takes about the same...
    if collection.count_documents(q.clone(), None).await? > 0 {
        collection.update_one(q.clone(), data.clone(), None).await?;
        println!(
            "Updated New City-time with key {:?} into collection: {}/{}",
            q, database, collection_name
        );
    } else {
        println!(
            "Inserted New City-time with key {} into collection: {}/{}",
            collection.insert_one(data.clone(), None).await?.inserted_id,
            database,
            collection_name
        );
    }
    Ok(())
}


