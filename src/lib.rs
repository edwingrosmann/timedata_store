#![warn(warnings)]
#![macro_use]
#![feature(const_fn)]

use crate::settings::{consumer_group, kafka_topic, set_args, set_test_mode};

pub mod kafka_api;
pub mod mongo_api;
pub mod settings;
pub mod time_and_date_data;

pub mod tests;

/// In order for this binary crate to run integration tests, both lib.rs AND main.rs have to be present.
/// main::main() call this function.
/// See more on this topic: https://doc.rust-lang.org/book/ch11-03-test-organization.html#integration-tests-for-binary-crates
pub fn main(test_mode: bool, args: Vec<String>) {
    set_args(args);
    set_test_mode(test_mode);

    kafka_api::consume_topic(&kafka_topic(), &consumer_group());
}
