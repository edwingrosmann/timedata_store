#![cfg(test)]
use crate::settings::{kafka_topic, DEFAULT_TOPIC, consumer_group, set_args, DEFAULT_CONSUMER_GROUP};
use crate::tests::test_util::args_for;

#[test]
fn kafka_topic_test() {
    set_args(args_for(vec!["kafka-topic=schmoffic"]));
    assert_eq!(&kafka_topic(), "schmoffic");
    set_args(args_for(vec!["topic=schmoffic"]));
    assert_eq!(&kafka_topic(), "schmoffic");
    set_args(args_for(vec!["KAFKA-TOPIC=schmoffic"]));
    assert_eq!(&kafka_topic(), "schmoffic");
    set_args(args_for(vec!["Topic=schmoffic"]));
    assert_eq!(&kafka_topic(), "schmoffic");
    set_args(args_for(vec!["topic"]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["kafka-topic="]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["topic="]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["Schmoppic"]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec!["kafka-topic:topic"]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
    set_args(args_for(vec![]));
    assert_eq!(&kafka_topic(), DEFAULT_TOPIC);
}

#[test]
fn consumer_group_test() {
    set_args(args_for(vec!["consumer-group=schmoffic"]));
    assert_eq!(&consumer_group(), "schmoffic");
    set_args(args_for(vec!["group=schmoffic"]));
    assert_eq!(&consumer_group(), "schmoffic");
    set_args(args_for(vec!["KAFKA-GROUP=schmoffic"]));
    assert_eq!(&consumer_group(), "schmoffic");
    set_args(args_for(vec!["Group=schmoffic"]));
    assert_eq!(&consumer_group(), "schmoffic");
    set_args(args_for(vec!["group"]));
    assert_eq!(&consumer_group(), DEFAULT_CONSUMER_GROUP);
    set_args(args_for(vec!["kafka-group="]));
    assert_eq!(&consumer_group(), DEFAULT_CONSUMER_GROUP);
    set_args(args_for(vec!["group="]));
    assert_eq!(&consumer_group(), DEFAULT_CONSUMER_GROUP);
    set_args(args_for(vec!["Schmoppic"]));
    assert_eq!(&consumer_group(), DEFAULT_CONSUMER_GROUP);
    set_args(args_for(vec!["kafka-group:group"]));
    assert_eq!(&consumer_group(), DEFAULT_CONSUMER_GROUP);
    set_args(args_for(vec![]));
    assert_eq!(&consumer_group(), DEFAULT_CONSUMER_GROUP);
}