use std::fs::read_to_string;
use std::process::Command as Cmd;
use std::time::Duration;

use kafka::client::KafkaClient;
use kafka::producer::{Producer, Record, RequiredAcks};

use crate::mongo_api::delete_by_key;
use crate::time_and_date_data::TimeData;

const TEST_TOPIC: &str = "test";
const TEST_GROUP: &str = "test-group";
const DOCUMENT_KEY: &str = "test";

pub fn default_args(topic: &str, group: &str) -> Vec<String> {
    vec![format!("topic={}", topic), format!("group={}", group)]
}

pub fn args_for(args: Vec<&str>) -> Vec<String> {
    args.iter().map(|s| s.to_string()).collect()
}

pub fn create_test_names() -> (String, String) {
    let session_id =
        &(time::OffsetDateTime::now_utc() - time::OffsetDateTime::unix_epoch()).whole_nanoseconds();
    (
        format!("{}-{}", TEST_TOPIC, session_id),
        format!("{}-{}", TEST_GROUP, session_id),
    )
}

pub fn setup(topic: &str) {
    create_kafka_topic(&topic);

    //One for MongoDB insert...
    write_message(
        DOCUMENT_KEY,
        read_to_string("src/tests/test-time-data.json").unwrap(),
        topic,
    );

    // One for MongoDB update...
    write_message(
        DOCUMENT_KEY,
        bson::to_bson(&TimeData::new())
            .unwrap()
            .into_relaxed_extjson()
            .to_string(),
        topic,
    );
}

pub fn cleanup(topic: &str, group: &str) {
    delete_kafka_topic(topic);
    delete_kafka_group(group);
    delete_by_key(DOCUMENT_KEY);
}

/// Create the Kafka-topic; make sure that the confluent-bin-directory  - which contains the "kafka-topics" command that will be used by this function -
/// has been set in the $PATH environment variable:
/// e.g. in file  ```~/.profile```
/// add/update line to ```export PATH="$HOME/.cargo/bin:$HOME/confluent-5.3.1/bin:$PATH"```
pub fn create_kafka_topic(topic: &str) {
    //Only continue when the topic does NOT yet exist...
    let mut client = KafkaClient::new(vec!["localhost:9092".to_owned()]);
    client.load_metadata_all().unwrap();

    if client.topics().contains(topic) {
        println!("Found Kafka-topic '{}'", topic);
    } else {
        match Cmd::new("kafka-topics")
            .arg("--create")
            .arg(format!("--topic={}", topic).as_str())
            .arg("--partitions=1")
            .arg("--replication-factor=1")
            .arg("--bootstrap-server=localhost:9092")
            .status()
        {
            Ok(_) => {
                println!("Created Kafka-topic: '{}'", topic);
            }
            Err(e) => {
                println!("Error creating Kafka-topic '{}': {}", topic, e);
            }
        }
    }
}

pub fn delete_kafka_topic(topic: &str) {
    //Only continue when the topic does NOT yet exist...
    let mut client = KafkaClient::new(vec!["localhost:9092".to_owned()]);
    client.load_metadata_all().unwrap();

    if client.topics().contains(topic) {
        match Cmd::new("kafka-topics")
            .arg("--delete")
            .arg(format!("--topic={}", topic).as_str())
            .arg("--bootstrap-server=localhost:9092")
            .status()
        {
            Ok(_) => {
                println!("Deleted Kafka-topic: '{}'", topic);
            }
            Err(e) => {
                println!("Error deleting Kafka-topic '{}': {}", topic, e);
            }
        }
    } else {
        println!("Kafka-topic already deleted.'{}'", topic);
    }
}

pub fn delete_kafka_group(group: &str) {
    //Only continue when the topic does NOT yet exist...
    let mut client = KafkaClient::new(vec!["localhost:9092".to_owned()]);
    client.load_metadata_all().unwrap();

    match Cmd::new("kafka-consumer-groups")
        .arg("--delete")
        .arg(format!("--group={}", group).as_str())
        .arg("--bootstrap-server=localhost:9092")
        .status()
    {
        Ok(_) => {
            println!("Deleted Kafka-Group: '{}'", group);
        }
        Err(e) => {
            println!("Error deleting Kafka-Group '{}': {}", group, e);
        }
    }
}

pub fn write_message(key: &str, data: String, topic: &str) {
    println!("-Sending message on topic '{}' with key '{}'", topic, key);
    producer()
        .send(&Record::from_key_value(topic, key, data))
        .expect("Failed writing test message")
}

fn producer() -> Producer {
    Producer::from_hosts(vec!["localhost:9092".to_owned()])
        .with_ack_timeout(Duration::from_secs(1))
        .with_required_acks(RequiredAcks::One)
        .create()
        .unwrap()
}
