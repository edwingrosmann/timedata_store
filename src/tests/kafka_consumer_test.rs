#![cfg(test)]
use crate::kafka_api::consume_topic;
use crate::tests::test_util::{cleanup, create_test_names, setup};

#[test]
fn consume_topic_test() {
    let (t, g) = create_test_names();

    setup(&t);
    consume_topic(&t, &g);
    cleanup(&t, &g);
}
