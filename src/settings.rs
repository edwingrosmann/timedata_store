use std::cell::RefCell;
use std::str::FromStr;

// A global variable, limited to the current Thread;
// In order to enable testing on the functions that probe the (immutable) env::args() collection, this contraption has been created:
// It is initialized with the env::args() collection and then the tests can add their test attributes to them...
thread_local!(
    static ARGS: RefCell<Vec<String>> = RefCell::new(Vec::new());
    static TEST_MODE: RefCell<bool> = RefCell::new(true);
);
pub const DEFAULT_TOPIC: &str = "time-data";
pub const DEFAULT_CONSUMER_GROUP: &str = "time-data-store";

pub fn set_args(new_args: Vec<String>) {
    ARGS.with(|args_cell| args_cell.replace(new_args));
    println!("Runtime Arguments: {:?}", args());
}

pub fn set_test_mode(test: bool) {
    TEST_MODE.with(|mode| mode.replace(test));
    println!("Test-mode: {}", test_mode());
}

pub fn args() -> Vec<String> {
    ARGS.with(|r| r.borrow().to_vec())
}

pub fn test_mode() -> bool {
    TEST_MODE.with(|m| m.to_owned().into_inner())
}

pub fn kafka_topic() -> String {
    attribute_value("topic", DEFAULT_TOPIC.to_string())
}

pub fn consumer_group() -> String {
    attribute_value("group", DEFAULT_CONSUMER_GROUP.to_string())
}

fn attribute_value<T: FromStr>(attribute_name: &str, default_value: T) -> T {
    match args()
        .iter()
        .find(|s| s.to_lowercase().contains(&format!("{}=", attribute_name)))
    {
        Some(key_value_string) => {
            let key_value: Vec<&str> = key_value_string.split('=').collect();
            if key_value.len() == 2 && !key_value[1].is_empty() {
                T::from_str(key_value[1]).unwrap_or(default_value)
            } else {
                default_value
            }
        }
        None => default_value,
    }
}