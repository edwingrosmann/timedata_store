pub use std::convert::From;
use std::fmt;
use std::str;
use std::str::FromStr;

use chrono::{FixedOffset, Weekday};
pub use http::Uri;
use mongodb::bson::Bson;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::{Error, Visitor};
use serde::export::Formatter;

#[derive(Debug, Clone, Deserialize)]
pub struct MessageKey {
    pub name: String,
    pub action: KafkaAction,
}

#[derive(Debug, Clone, Deserialize)]
pub enum KafkaAction {
    Upsert,
    Delete,
}


#[derive(Serialize, Deserialize)]
pub struct TimeData {
    pub _id: String,
    pub page_uri: UriWrapper,
    pub node_data: String,
    pub city_times: Vec<CityData>,
    pub last_updated: String,
    pub sort: Sort,
    pub hash: String,
    pub count: i32,
}

#[derive(Serialize, Deserialize)]
pub struct CityData {
    pub id: i32,
    pub name: String,
    pub time_string: String,
    pub utc_offset: UtcOffset,
    pub is_dls: bool,
    pub url: String,
    pub sort: Sort,
}

pub struct UriWrapper(Uri);

pub struct DayOfWeek(Weekday);

pub struct UtcOffset(FixedOffset);

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Sort {
    ByName,
    ByOffset,
}

impl TimeData {
    pub fn new() -> Self {
        TimeData {
            _id: String::default(),
            node_data: "".to_string(),
            city_times: vec![CityData::new()],
            sort: Sort::ByName,
            last_updated: String::default(),
            page_uri: UriWrapper(Uri::default()),
            hash: String::default(),
            count:0,
        }
    }
}

impl CityData {
    fn new() -> Self {
        CityData {
            id: 0,
            name: "".to_string(),
            time_string: "".to_string(),
            utc_offset: UtcOffset::from(FixedOffset::west(0)),
            is_dls: false,
            url: "".to_string(),
            sort: Sort::ByName,
        }
    }
}

impl UriWrapper {
    pub fn from(uri: &str) -> Self {
        UriWrapper(Uri::from_str(uri).unwrap())
    }
}

impl Serialize for UriWrapper {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
        where
            S: Serializer,
    {
        Bson::String(self.0.to_string()).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for UriWrapper {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        struct UriWrapperVisitor;

        impl<'de> Visitor<'de> for UriWrapperVisitor {
            type Value = UriWrapper;

            fn expecting(&self, _: &mut Formatter) -> fmt::Result { unimplemented!() }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
                where
                    E: Error,
            {
                Ok(UriWrapper::from(s))
            }

            fn visit_borrowed_str<E>(self, s: &str) -> Result<Self::Value, E>
                where
                    E: Error,
            {
                self.visit_str::<E>(s)
            }
        }
        deserializer.deserialize_str(UriWrapperVisitor)
    }

    fn deserialize_in_place<D>(
        _deserializer: D,
        _place: &mut Self,
    ) -> Result<(), <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        unimplemented!()
    }
}

impl Serialize for UtcOffset {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
        where
            S: Serializer,
    {
        Bson::String(self.0.to_string()).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for UtcOffset {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        struct UtcOffsetVisitor;

        impl<'de> Visitor<'de> for UtcOffsetVisitor {
            type Value = UtcOffset;

            fn expecting(&self, _: &mut Formatter) -> fmt::Result { unimplemented!() }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
                where
                    E: Error,
            {
                Ok(FixedOffset::east(
                    time::UtcOffset::parse(&s.replace(":", ""), "%z")
                        .unwrap_or_else(|_| panic!("Not a valid UTC-Offset string: {}", &s))
                        .as_seconds(),
                )
                    .into())
            }

            fn visit_borrowed_str<E>(self, s: &str) -> Result<Self::Value, E>
                where
                    E: Error,
            {
                self.visit_str::<E>(s)
            }
        }
        deserializer.deserialize_str(UtcOffsetVisitor)
    }

    fn deserialize_in_place<D>(
        _deserializer: D,
        _place: &mut Self,
    ) -> Result<(), <D as Deserializer<'de>>::Error>
        where
            D: Deserializer<'de>,
    {
        unimplemented!()
    }
}

impl From<FixedOffset> for UtcOffset {
    fn from(d: FixedOffset) -> Self {
        UtcOffset(d)
    }
}
