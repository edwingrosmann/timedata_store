use timedata_store as store;
use timedata_store::tests::test_util::{cleanup, create_test_names, default_args, setup};

#[test]
fn main_test() {
    //Create a test-topic- and test-group-name...
    let (t, g) = create_test_names();

    setup(&t);
    store::main(true, default_args(&t, &g));
    cleanup(&t, &g);
}

//#[test]
//fn failing_data_test() {
//    //Create a test-topic- and test-group-name...
//    let (t, g) = create_test_names();
//
//    setup(&t);
//    store::main(true, default_args(&t, &g));
//    cleanup(&t, &g);
//}
