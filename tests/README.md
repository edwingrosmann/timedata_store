### Running the integration Tests

The 'tests'-directory sitting next to the 'src'-directory is _Home of the integration tests_.
In order for this binary crate to run integration tests, both lib.rs AND main.rs have to be present.
See more on this topic: https://doc.rust-lang.org/book/ch11-03-test-organization.html#integration-tests-for-binary-crates

To run the integration tests ONLY:
```cargo test --test e2e_tests  -- --nocapture```

### Requirements

The box that runs these integration tests needs to have:
* Internet connectivity.
* Rust Nightly
* This crate installed: 
```git clone https://bitbucket.org/edwingrosmann/rusttimeanddatedotcomparser.git)``` or 
```git clone https://github.com/edwingrosmann/rusttimeanddatedotcomparser.git``` (not neccesarily up-to-date)
* MongoDB installed; no fancy authorisation required.
* Kafka Confluent installed; make sure the confluent-bin directory is in the file-path e.g. add it to ```$FILE``` in ```.profile 
