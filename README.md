###Consuming messages from a Kafka-topic and storing them in a MongoDB


###Code Coverage

#### Please check out the [Integration Tests instructions](tests/README.md)  if you want to try run 'rgcov' Code Coverage on this crate.


####Set environment variables: e.g. create an alias in ```.bashrc```
```
alias rust-coverage-env='export CARGO_INCREMENTAL=0;export RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=unwind";export RUSTDOCFLAGS="-Cpanic=abort"'
```
the call alias ```rust-coverage-env```

#### Run these commands
```
cargo clean -p timedata_store ; cargo +nightly test --workspace -- --nocapture
grcov ./target/debug/  -t html --llvm  --ignore-not-existing -o ./target/debug/coverage/
```
#### Open report in browser
Located at ```target/debug/coverage/index.html```

![](Screenshot_2020-09-03%20Grcov%20report%20—src.png)